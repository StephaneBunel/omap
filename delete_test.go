package omap

import (
	"fmt"
	"testing"
)

func Test_Delete(t *testing.T) {
	om := NewOmapTest(t)
	wantLen := om.Len()

	om.Delete("deux")
	wantLen--
	gotLen := om.Len()

	if gotLen != wantLen {
		t.Errorf("want %v, got %v", wantLen, gotLen)
		return
	}

	gotKeys := om.ToKey(0, 1, 2, 3)
	for i, wantKey := range []string{"un", KeyUndefined, "trois", "quatre"} {
		if gotKeys[i] != wantKey {
			t.Errorf("want %v, got %v", wantKey, gotKeys[i])
		}
	}

	gotRatio := om.DeleteRatio()
	wantRatio := float32(1.0 / 4.0)
	if gotRatio != wantRatio {
		t.Errorf("want %v, got %v", wantRatio, gotRatio)
	}

	om.Compact(0) // Force compaction

	gotKeys = om.ToKey(0, 1, 2)
	for i, wantKey := range []string{"un", "trois", "quatre"} {
		if gotKeys[i] != wantKey {
			t.Errorf("want %v, got %v", wantKey, gotKeys[i])
		}
	}

	om.Delete("dix")
	om.Delete(10)
	om.Delete(2)
}

func Test_DeletePanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	om := NewOmapTest(t)
	om.Delete(true)
	t.Errorf("The code did not panic")
}

func Test_CompactEmpty(t *testing.T) {
	om := New()
	om.DeleteRatio()
	om.Compact(0)
}
