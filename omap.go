package omap

import (
	"errors"
	"sync"
)

// OMap object.
type OMap struct {
	// options    int
	accessLock sync.Mutex
	store      itemStore
	// observers  []ObserverChan
}

// VisiterFn is the visitor function signature.
type VisiterFn func(idx int, itm Item)

const (
	KeyUndefined   = ""
	IndexUndefined = -1
)

// Package errors.
var (
	ErrParameterType = errors.New("parameter(s) must be of type int or string")
)

// New returns a new OMap.
func New() *OMap {
	om := new(OMap)
	om.store.items = []Item{}
	om.store.key2index = map[string]int{}

	return om
}
