package omap

// Set adds or updates item(s)
// If item.Key == KeyUndefined then item is skipped.
func (om *OMap) Set(items ...Item) *OMap {
	var idx int
	var exists bool

	om.accessLock.Lock()
	defer om.accessLock.Unlock()

	for _, itm := range items {
		if itm.Key == KeyUndefined {
			continue
		}
		if idx, exists = om.store.key2index[itm.Key]; exists {
			om.store.items[idx].Value = itm.Value
		} else {
			om.store.items = append(om.store.items, itm)
			idx = len(om.store.items) - 1
			om.store.key2index[itm.Key] = idx
		}
	}

	return om
}
