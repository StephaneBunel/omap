package omap

import (
	"testing"
)

func Test_Set(t *testing.T) {
	om := New().Set(NewItem("", "empty"))

	wantLen := 0
	gotLen := om.Len()
	if wantLen != gotLen {
		t.Errorf("want %v, got %v", wantLen, gotLen)
	}

	om.Set(NewItem("un", 0)).Set(NewItem("un", 1))
	item := om.Get("un")[0]

	gotValue := item.Value
	wantValue := 1
	if gotValue != wantValue {
		t.Errorf("want %v, got %v", wantValue, gotValue)
	}
}
