package omap

import "testing"

func Test_Visit(t *testing.T) {
	om := NewOmapTest(t)

	om.Visit(func(idx int, itm Item) {
		gotKey := itm.Key
		wantKey := testKeys[idx]
		if wantKey != gotKey {
			t.Errorf("want %v, got %v", wantKey, gotKey)
		}
	})

}

func Test_ItemString(t *testing.T) {
	item := NewItem("un", 1)

	wantString := `Item{ Key="un", Value=1 }`
	gotString := item.String()
	if wantString != gotString {
		t.Errorf("want %v, got %v", wantString, gotString)
	}
}
