package omap

// Len returns length of the map.
func (om *OMap) Len() int {
	return len(om.store.key2index)
}

// Visit implements the visitor.
// Passed function if called with (index, Item) for each Item.
func (om *OMap) Visit(visitor VisiterFn) {
	for idx, itm := range om.store.items {
		if !itm.delete {
			visitor(idx, itm)
		}
	}
}
