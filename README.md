[![pipeline status](https://gitlab.com/StephaneBunel/omap/badges/master/pipeline.svg)](https://gitlab.com/StephaneBunel/omap/-/commits/master)
[![coverage report](https://gitlab.com/StephaneBunel/omap/badges/master/coverage.svg)](https://gitlab.com/StephaneBunel/omap/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/StephaneBunel/omap)](https://goreportcard.com/report/gitlab.com/StephaneBunel/omap)

# OMAP

Package omap provide an ordered map of items.
Item is a key / value pair.
Key is a string and unique in the map.
Value can be of any type.
Item can be accessed by is's Key or Index.

This implementation use an array of items to remember order of insertion and a map to quickly link key with item.
A key can be deleted without changing index number nor order of other items until you ask for a compaction.

## Examples

[ex1.go](examples/ex1.go)

## Documentation

[Doc](doc.md)
