package omap

// Get returns item(s) by it's key or index
// If key or index doesn't corresponds to an item then returned item is empty (item.IsEmpty() == true).
func (om *OMap) Get(multiIndexOrKey ...interface{}) []Item {
	r := make([]Item, 0, len(multiIndexOrKey))

	for _, indexOrKey := range multiIndexOrKey {
		switch v := indexOrKey.(type) {
		case string:
			key := v
			if idx, exists := om.store.key2index[key]; exists {
				r = append(r, om.store.items[idx])
			} else {
				r = append(r, Item{})
			}
		case int:
			idx := v
			if idx >= 0 && idx < len(om.store.items) {
				r = append(r, om.store.items[idx])
			} else {
				r = append(r, Item{})
			}
		default:
			panic(ErrParameterType)
		}
	}

	return r
}

// Exists returns an array of boolean.
// Each one is true when key or index exists, else false.
func (om *OMap) Exists(multiIndexOrKey ...interface{}) []bool {
	r := make([]bool, 0, len(multiIndexOrKey))

	for _, indexOrKey := range multiIndexOrKey {
		switch v := indexOrKey.(type) {
		case string:
			key := v
			_, exists := om.store.key2index[key]
			r = append(r, exists)
		case int:
			idx := v
			if idx < 0 || idx >= len(om.store.items) {
				r = append(r, false)
			} else {
				r = append(r, !om.store.items[idx].delete)
			}
		default:
			panic(ErrParameterType)
		}
	}

	return r
}

// ToIndex returns array of index according to passed key(s).
func (om *OMap) ToIndex(keys ...string) []int {
	om.accessLock.Lock()
	defer om.accessLock.Unlock()

	r := make([]int, 0, len(keys))
	for _, key := range keys {
		if idx, exists := om.store.key2index[key]; exists {
			r = append(r, idx)
		} else {
			r = append(r, -1)
		}
	}

	return r
}

// ToKey returns array of key according to passed index(s).
func (om *OMap) ToKey(indexes ...int) []string {
	om.accessLock.Lock()
	defer om.accessLock.Unlock()

	r := make([]string, 0, len(indexes))
	for _, idx := range indexes {
		if idx < 0 || idx >= len(om.store.items) {
			r = append(r, "")

			continue
		}
		if om.store.items[idx].delete {
			r = append(r, "")

			continue
		}
		r = append(r, om.store.items[idx].Key)
	}

	return r
}
