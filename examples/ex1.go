package main

import (
	"fmt"

	"gitlab.com/StephaneBunel/omap/v2"
)

func main() {
	om := omap.New()

	om.Set(
		omap.Item{Key: "test0", Value: "test-value-0"},
		omap.NewItem("test1", "test-value-1"),
	)

	items := om.Get(0, "test1", 12)
	fmt.Printf("%v\n", items)       // [Item{ Key="test0", Value=test-value-0 } Item{ Key="test1", Value=test-value-1 } Item{ Key="", Value=<nil> }]
	fmt.Println(items[2].IsEmpty()) // true

	keys := om.ToKey(0, 1, 2)
	fmt.Printf("%#v\n", keys)                 // []string{"test0", "test1", ""}
	fmt.Println(keys[2] == omap.KeyUndefined) // true

	indexes := om.ToIndex("test1", "test0", "unknown")
	fmt.Printf("%#v\n", indexes)                   // []int{1, 0, -1}
	fmt.Println(indexes[2] == omap.IndexUndefined) // true

	om.Delete(0)

	om.Visit(func(idx int, itm omap.Item) {
		fmt.Printf("#%d -> %q\n", idx, itm.Key)
	})
	// #1 -> "test1"

	dr := om.DeleteRatio()
	fmt.Println(dr) // 0.5

	om.Compact(0.33) // Compact if DeleteRatio >= 33%
	dr = om.DeleteRatio()
	fmt.Println(dr) // 0

	// After compaction item index are renumbered
	om.Visit(func(idx int, itm omap.Item) {
		fmt.Printf("#%d -> %q\n", idx, itm.Key)
	})
	// #0 -> "test1"
}
