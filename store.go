package omap

// itemStore is the OMap internal items storage.
type itemStore struct {
	items     []Item
	key2index map[string]int
}
