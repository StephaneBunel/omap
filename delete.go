package omap

// Delete deletes item. You can pass a key (string) or an index (int).
// To avoid cumbersome memory copy and index recalculation, item is released and marked as deleted.
// See Compact() to clean up deleted items.
func (om *OMap) Delete(intOrString interface{}) {
	om.accessLock.Lock()
	defer om.accessLock.Unlock()

	switch v := intOrString.(type) {
	case string:
		key := v
		if idx, exists := om.store.key2index[key]; exists {
			om.delete(idx)
		}
	case int:
		idx := v
		if idx >= 0 && idx < len(om.store.items) {
			om.delete(idx)
		}
	default:
		panic(ErrParameterType)
	}
}

func (om *OMap) delete(index int) {
	delete(om.store.key2index, om.store.items[index].Key)
	om.store.items[index].delete = true
	om.store.items[index].Key = KeyUndefined
	om.store.items[index].Value = nil
}

// Compact compacts the Omap according to the minimalRatio threshold.
// If DeleteRatio() >= minimalRatio then the Omap will be compacted.
// Compact() move all items not marked as deleted into a new array and destroy the old one.
// Indexes are updated. Keep in mind that compaction reset indexes.
// After compaction DeleteRatio() is 0 as expected.
// Use Compact(0) to force compaction.
func (om *OMap) Compact(minimalRatio float32) {
	if len(om.store.key2index) == 0 {
		// All items is marked deleted.
		// By creating a brand new empty array, all old items will be garbage collected
		om.store.items = []Item{}

		return
	}

	if om.DeleteRatio() >= minimalRatio {
		om.compact()
	}
}

func (om *OMap) compact() {
	om.accessLock.Lock()
	defer om.accessLock.Unlock()

	newitems := make([]Item, 0, len(om.store.key2index))
	for _, itm := range om.store.items {
		if itm.delete {
			continue
		}
		newitems = append(newitems, itm)
		om.store.key2index[itm.Key] = len(newitems) - 1
	}
	om.store.items = newitems
}

// DeleteRatio returns the current ratio of deleted items as a float between [0..1].
// 0 == No item marked as deleted. 1 == All items are marked as deleted.
func (om *OMap) DeleteRatio() float32 {
	if itemsLen, keyInMap := float32(len(om.store.items)), float32(len(om.store.key2index)); itemsLen > 0 {
		return float32(1) - (keyInMap / itemsLen)
	}

	return 0
}
