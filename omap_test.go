package omap

import (
	"testing"
)

var testKeys = []string{"un", "deux", "trois", "quatre"}
var testValues = []int{1, 2, 3, 4}
var testNumbers = func() []Item {
	n := make([]Item, len(testKeys))
	for i, key := range testKeys {
		n[i] = NewItem(key, testValues[i])
	}
	return n
}()

func NewOmapTest(t *testing.T) *OMap {
	om := New().Set(testNumbers...)
	if om.Len() != 4 {
		t.Errorf("want: %v, got: %v", 4, om.Len())
	}
	return om
}
