package omap

import (
	"fmt"
	"testing"
)

func Test_Get(t *testing.T) {
	om := NewOmapTest(t)

	items := om.Get("trois", "quatre", "cinq")

	wantLen := int(3)
	gotLen := len(items)
	if gotLen != wantLen {
		t.Errorf("want len() == %v, got %v", wantLen, gotLen)
		return
	}

	wantItem := NewItem("trois", 3)
	gotItem := items[0]
	if gotItem.Key != wantItem.Key || gotItem.Value != wantItem.Value {
		t.Errorf("want %v, got %v", wantItem, gotItem)
	}

	wantItem = NewItem("quatre", 4)
	gotItem = items[1]
	if gotItem.Key != wantItem.Key || gotItem.Value != wantItem.Value {
		t.Errorf("want %v, got %v", wantItem, gotItem)
	}

	gotItem = items[2]
	wantEmpty := true
	gotEmpty := gotItem.IsEmpty()
	if gotEmpty != wantEmpty {
		t.Errorf("want %v, got %v", wantEmpty, gotEmpty)
	}
}

func Test_Get2(t *testing.T) {
	om := NewOmapTest(t)

	gotItems := om.Get(1, 3, 10)

	gotKey := gotItems[0].Key
	wantKey := "deux"
	if gotKey != wantKey {
		t.Errorf("want %v, got %v", wantKey, gotKey)
	}

	gotKey = gotItems[1].Key
	wantKey = "quatre"
	if gotKey != wantKey {
		t.Errorf("want %v, got %v", wantKey, gotKey)
	}

	gotKey = gotItems[2].Key
	wantKey = KeyUndefined
	if gotKey != wantKey {
		t.Errorf("want %v, got %v", wantKey, gotKey)
	}
}

func Test_Exists(t *testing.T) {
	om := NewOmapTest(t)

	gotExists := om.Exists("un", "dix", 0, 10)
	wantExists := []bool{true, false, true, false}
	for i, wanted := range wantExists {
		if wanted != gotExists[i] {
			t.Errorf("want %v, got %v", wanted, gotExists[i])
		}
	}
}

func Test_ExistsPanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	om := NewOmapTest(t)
	om.Exists(true)
	t.Errorf("The code did not panic")
}

func Test_GetPanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r)
		}
	}()

	om := NewOmapTest(t)
	om.Get(true)
	t.Errorf("The code did not panic")
}

func Test_ToKey(t *testing.T) {
	om := NewOmapTest(t)

	gotKeys := om.ToKey(0, 1, 2, 3, 10)
	for i, wantKey := range []string{"un", "deux", "trois", "quatre", KeyUndefined} {
		if gotKeys[i] != wantKey {
			t.Errorf("want %v, got %v", wantKey, gotKeys[i])
		}
	}
}

func Test_ToIndex(t *testing.T) {
	om := NewOmapTest(t)

	gotIndex := om.ToIndex("un", "deux", "trois", "quatre", "dix")
	for i, wantIndex := range []int{0, 1, 2, 3, IndexUndefined} {
		if gotIndex[i] != wantIndex {
			t.Errorf("want %v, got %v", wantIndex, gotIndex[i])
		}
	}
}
