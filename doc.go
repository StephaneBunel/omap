// Copyright 2020 Stéphane Bunel. All rights reserved.
// Use of this source code is governed by the MIT
// License that can be found in the LICENCE file.

/*
Package omap provide an ordered map of items.
Item is a key / value pair.
Key is a string and unique in the map.
Value can be of any type.
Item can be accessed by is's Key or Index.

This implementation use an array of items to remember order of insertion and a map to quickly link key with item.
A key can be deleted without changing index number nor order of other items until you ask for a compaction.
*/
package omap
