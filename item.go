package omap

import "fmt"

type Item struct {
	Key    string
	Value  interface{}
	delete bool
}

// NewItem returns a new Item object initialized with given key and value.
func NewItem(key string, value interface{}) Item {
	return Item{Key: key, Value: value, delete: false}
}

// String returns an Item string representation.
func (itm Item) String() string {
	return fmt.Sprintf("Item{ Key=%q, Value=%v }", itm.Key, itm.Value)
}

// IsEmpty returns true if the item's Key is KeyUndefined.
func (itm Item) IsEmpty() bool {
	return itm.Key == ""
}
